PermJack: permission management for Drupal made easy
=================================

The goal of this module is to make it easier to manage permissions in code.
This module creates a simple hook that allows users to specify which permissions
they would like to grant to certain roles.

Every time caches are flushed or the "Save" button is pressed on the permissions
admin page this hook is run and all permissions defined in the user's code are
reset.

Implementing the hook is simple:

- Define a function called `MY_MODULE_permjack` where `MY_MODULE` is the name of
the module you create to implement the hook.
- Make sure this function returns an associative array indexed by role names
with corresponding permissions as arrays of permission machine names.

Attention users of the "Features" module!!
Please note that managing permissions in code can override some pre-existing
features. This module currently does NOT deal with this in any way, so the user
should be prepared to tackle such problems themselves.