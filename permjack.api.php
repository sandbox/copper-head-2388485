<?php
/**
 * Introduces a hook that can be implemented by other modules.
 * @return array An array indexed by user role names with arrays of permission
 * strings as values.
 */
function hook_permjack(){
  // This simple example uses just one role machine name, but you can have as
  // many of these as you like, as long as you pair each of them up with some
  // permission array (it can be the same array for multiple roles).
  $role_name = 'content moderator';
  // array of permissions strings. These can be found by enabling the Devel module
  // and configuring it to display permission machine names in the admin page
  // for managing permissions. Once this is done, hover your mouse over any
  // permission of interest to you and you will see it's permission string or
  // machine name appear.
  $permissions = array(
    'administer blocks',
    'access comments'
    );
  // make sure to return an array where roles are mapped onto their permissions.
  return array(
    $role_name => $permissions
    );
}